<?php

namespace Modules\Customer\Repositories;

use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Modules\Customer\Entities\Customer;

class CustomerRepository extends BaseRepository implements CustomerRepositoryInterface
{

    /**
     * model
     *
     * @var Customer
     */
    protected $model;

    public function __construct(Customer $customer)
    {
        $this->model = $customer;
    }

    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }


    /**
     * getItems
     *
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getItemsBuilder(Request $request)
    {

        $query = $this->getEloquentBuilder();
        if ($search = $request->search) {
            $query->where('name', 'like', '%' . $search . '%');
        }

        return $query;
    }
}
